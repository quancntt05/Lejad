# Learn Java Advantage Course - Day Report

This is report about my course (Learn Java Advantage - 6 months). It is divided into 4 modules, and it will be reported day by day.

(Because I write it when module II was start,so module I not have)

##Module II

**2017 Oct 25** 

- Start day 4 - lesson 4 "XML DOM"

**2017 Oct 28** 

- Update Exercise 4.2 v0.2

**2017 Oct 29** 

- Start day 5 - lesson 5 "Database"
- Update Exercise 4.2 v1.0

**2017 Oct 30**

- Update README.md
- Update Exercise 4.2 v1.1
- Add Exercise 5.3 v1.0

**2017 Oct 31**

- Update README.md
- Add Exercise 5.3 v1.1
- Add Exercise 5.3 v1.0