package module_2_day7_20171105;

public class Signleton {
	private Signleton() {
	}
	private static Signleton signleton = new Signleton();
	public static Signleton getInstance() {
		return signleton;
	}
}
