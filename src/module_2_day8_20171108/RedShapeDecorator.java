package module_2_day8_20171108;

public class RedShapeDecorator implements Shape {
	private Shape shape;
	public RedShapeDecorator(Shape shape) {
		// TODO Auto-generated constructor stub
		this.shape = shape;
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		shape.draw();
		System.out.println("Border Color: Red");
		
	}

}
