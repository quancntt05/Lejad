package module_2_day8_20171108;

public class ShapeFactory {
	public Shape getShape(String s) {
		Shape shape = null;
		if (s.equals("Circle")) {
			shape = new Circle();
		}else if (s.equals("Square")){
			shape = new Square();
			}
		return shape;
		}
		
}

