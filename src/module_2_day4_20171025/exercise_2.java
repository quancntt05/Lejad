package module_2_day4_20171025;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.corba.se.impl.orbutil.graph.Node;

import javax.swing.JTabbedPane;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class exercise_2 extends JFrame {

	private JPanel contentPane;
	private JTextField txtFullName;
	private JTextField txtBirthday;
	private JTextField txtSalary;
	private JTextField txtAddress;
	private JTable table;
	private final String XML_PATH = "D:\\txt\\info_staff.xml";
	private Document document; //document for write
	private Document doc; //document for read
	private int idx = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					exercise_2 frame = new exercise_2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	public exercise_2() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		
		document = builder.newDocument();
		Element root = document.createElement("info_staff");
		document.appendChild(root);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 776, 486);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(52, 29, 652, 358);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Add new staff", null, panel, null);
		panel.setLayout(null);
		
		String[] dept = {"Developer","Tester","Auditer"};
		JComboBox cbbDept = new JComboBox(dept);
		cbbDept.setSelectedIndex(0);
		cbbDept.setBounds(196, 25, 196, 22);
		panel.add(cbbDept);
		
		JLabel lblDepament = new JLabel("Department");
		lblDepament.setBounds(97, 28, 77, 16);
		panel.add(lblDepament);
		
		JLabel lblFullName = new JLabel("Full Name");
		lblFullName.setBounds(97, 77, 77, 16);
		panel.add(lblFullName);
		
		txtFullName = new JTextField();
		txtFullName.setBounds(196, 74, 196, 22);
		panel.add(txtFullName);
		txtFullName.setColumns(10);
		
		JLabel lblBir = new JLabel("Birthday");
		lblBir.setBounds(97, 169, 77, 16);
		panel.add(lblBir);
		
		txtBirthday = new JTextField();
		txtBirthday.setColumns(10);
		txtBirthday.setBounds(196, 166, 196, 22);
		panel.add(txtBirthday);
		
		
		JLabel lblGrender = new JLabel("Gender");
		lblGrender.setBounds(97, 122, 77, 16);
		panel.add(lblGrender);
		
		String[] gender = {"Male","Female"};
		JComboBox cbbGender = new JComboBox(gender);
		cbbGender.setSelectedIndex(0);
		cbbGender.setBounds(196, 119, 97, 22);
		panel.add(cbbGender);
		
		JLabel label = new JLabel("Salary");
		label.setBounds(97, 214, 77, 16);
		panel.add(label);
		
		txtSalary = new JTextField();
		txtSalary.setColumns(10);
		txtSalary.setBounds(196, 211, 196, 22);
		panel.add(txtSalary);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(97, 261, 77, 16);
		panel.add(lblAddress);
		
		txtAddress = new JTextField();
		txtAddress.setColumns(10);
		txtAddress.setBounds(196, 258, 196, 22);
		panel.add(txtAddress);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				idx++;
				Element deptm = document.createElement((String) cbbDept.getSelectedItem());
				root.appendChild(deptm);	
				
				Element id = document.createElement("ID");
				id.appendChild(document.createTextNode(String.valueOf(idx)));
				deptm.appendChild(id);
				
				Element fullName = document.createElement("fullname");
				fullName.appendChild(document.createTextNode(txtFullName.getText()));
				deptm.appendChild(fullName);
				
				Element gender = document.createElement("gender");
				gender.appendChild(document.createTextNode((String) cbbGender.getSelectedItem()));
				deptm.appendChild(gender);
				
				Element birthday = document.createElement("birthday");
				birthday.appendChild(document.createTextNode(txtBirthday.getText()));
				deptm.appendChild(birthday);
				
				Element salary = document.createElement("salary");
				salary.appendChild(document.createTextNode(txtSalary.getText()));
				deptm.appendChild(salary);
				
				Element address = document.createElement("address");
				address.appendChild(document.createTextNode(txtAddress.getText()));
				deptm.appendChild(address);
				
				new JOptionPane().showMessageDialog(rootPane, "Added");
			}
		});
		btnAdd.setBounds(124, 290, 97, 25);
		panel.add(btnAdd);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					TransformerFactory factory = TransformerFactory.newInstance();
					Transformer transformer = factory.newTransformer();
					DOMSource domSource = new DOMSource(document);
					StreamResult result = new StreamResult(new File(XML_PATH));
					
					transformer.transform(domSource, result);
					new JOptionPane().showMessageDialog(rootPane, "Saved");
				} catch (Exception e2) {
					new JOptionPane().showMessageDialog(rootPane, "Not Save");
				}
			}
		});
		btnSave.setBounds(258, 290, 97, 25);
		panel.add(btnSave);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Report", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel label_1 = new JLabel("Department");
		label_1.setBounds(130, 34, 77, 16);
		panel_1.add(label_1);
		
		JScrollPane jScrollPane = new JScrollPane();
		jScrollPane.setBounds(39, 89, 567, 213);
		panel_1.add(jScrollPane);
		
		table = new JTable();
		jScrollPane.setViewportView(table);
		
		JComboBox cbbDeptR = new JComboBox(dept);
		cbbDeptR.setSelectedIndex(-1);
		cbbDeptR.addActionListener(e -> {
			try {
				doc = builder.parse(new File(XML_PATH));
				DefaultTableModel model = new DefaultTableModel();
				model.addColumn("ID");
				model.addColumn("Full name");
				model.addColumn("Gender");
				model.addColumn("Birthday");
				model.addColumn("Salary");
				model.addColumn("Address");
									
				NodeList lNote = doc.getElementsByTagName((String) cbbDeptR.getSelectedItem());
				for (int i = 0; i < lNote.getLength(); i++) {
					org.w3c.dom.Node nNode = lNote.item(i);
					if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;
						
						model.addRow(new Object[]{eElement.getElementsByTagName("ID").item(0).getTextContent(),
												  eElement.getElementsByTagName("fullname").item(0).getTextContent(),
												  eElement.getElementsByTagName("gender").item(0).getTextContent(),
												  eElement.getElementsByTagName("birthday").item(0).getTextContent(),
												  eElement.getElementsByTagName("salary").item(0).getTextContent(),
												  eElement.getElementsByTagName("address").item(0).getTextContent(),												  
													});
	
					}
				}
				table.setModel(model);
			} catch (Exception e2) {
				// TODO: handle exception
			}

		});
		cbbDeptR.setBounds(229, 31, 196, 22);
		panel_1.add(cbbDeptR);
		
		
	}
}
