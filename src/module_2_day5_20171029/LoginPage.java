package module_2_day5_20171029;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import com.mysql.cj.api.mysqla.result.Resultset;
import com.mysql.cj.jdbc.PreparedStatement;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginPage extends JFrame {

	private JPanel contentPane;
	private JTextField txtUser;
	private JTextField txtPass;
	private LoginPage that = this;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginPage frame = new LoginPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginPage() {
		ConnectDB db = new ConnectDB();
		Connection connect = db.connect();
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				db.disconnect();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 531, 367);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLoginPage = new JLabel("LOGIN PAGE");
		lblLoginPage.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblLoginPage.setHorizontalAlignment(SwingConstants.CENTER);
		lblLoginPage.setBounds(170, 24, 139, 27);
		contentPane.add(lblLoginPage);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(153, 75, 58, 16);
		contentPane.add(lblUsername);
		
		txtUser = new JTextField();
		txtUser.setBounds(243, 72, 116, 22);
		contentPane.add(txtUser);
		txtUser.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(153, 130, 58, 16);
		contentPane.add(lblPassword);
		
		JPasswordField passwordField = new JPasswordField();
		passwordField.setColumns(10);
		passwordField.setBounds(243, 127, 116, 22);
		contentPane.add(passwordField);
		
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String sql = "SELECT * from admin_info where username = ? AND password = ?";
				try {
					java.sql.PreparedStatement statement = connect.prepareStatement(sql);
					statement.setString(1, txtUser.getText());
					statement.setString(2, getMD5(passwordField.getText()));
					ResultSet resultset = statement.executeQuery();
					if (resultset.next()) {
						new JOptionPane().showMessageDialog(rootPane, "Login successfully");
						that.setVisible(false);
						ControlBookPage controlBookPage = new ControlBookPage();
						controlBookPage.setVisible(true);
					} else {
						new JOptionPane().showMessageDialog(rootPane, "Invalid Username or Password");
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					new JOptionPane().showMessageDialog(rootPane, "Invalid Username or Password");
				}
				
			}

			private String getMD5(String input) {
				// TODO Auto-generated method stub
		        try {
		            MessageDigest md = MessageDigest.getInstance("MD5");
		            byte[] messageDigest = md.digest(input.getBytes());
		            BigInteger number = new BigInteger(1, messageDigest);
		            String hashtext = number.toString(16);
		            // Now we need to zero pad it if you actually want the full 32 chars.
		            while (hashtext.length() < 32) {
		                hashtext = "0" + hashtext;
		            }
		            return hashtext;
		        }
		        catch (NoSuchAlgorithmException e) {
		            throw new RuntimeException(e);
		        }
			}
		});
		btnLogin.setBounds(199, 183, 97, 25);
		contentPane.add(btnLogin);
	}
}
