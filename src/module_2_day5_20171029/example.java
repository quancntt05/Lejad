package module_2_day5_20171029;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.cj.jdbc.PreparedStatement;

public class example {
	public static void main(String[] args) throws SQLException {
		ConnectDB connectDB = new ConnectDB();
		Connection connect = connectDB.connect();
		PreparedStatement statement = (PreparedStatement) connect.prepareStatement("select * from admin_info;");
		ResultSet resultset = statement.executeQuery();
		
		while(resultset.next()){
			System.out.println(resultset.getString(1));
			System.out.println(resultset.getString(2));
		}
	}

}
