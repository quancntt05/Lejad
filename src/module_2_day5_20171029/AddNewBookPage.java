package module_2_day5_20171029;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class AddNewBookPage extends JFrame {

	private JPanel contentPane;
	private JTextField txtNameBook;
	private JTextField txtAuthor;
	private JTextField txtPublisher;
	private JTextField txtPrice;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddNewBookPage frame = new AddNewBookPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddNewBookPage() {
		ConnectDB db = new ConnectDB();
		Connection connect = db.connect();
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				db.disconnect();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBooksName = new JLabel("Book's name");
		lblBooksName.setBounds(58, 16, 79, 16);
		contentPane.add(lblBooksName);
		
		txtNameBook = new JTextField();
		txtNameBook.setBounds(149, 13, 199, 22);
		contentPane.add(txtNameBook);
		txtNameBook.setColumns(10);
		
		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(58, 59, 79, 16);
		contentPane.add(lblAuthor);
		
		txtAuthor = new JTextField();
		txtAuthor.setColumns(10);
		txtAuthor.setBounds(149, 56, 199, 22);
		contentPane.add(txtAuthor);
		
		JLabel lblProducer = new JLabel("Publisher");
		lblProducer.setBounds(58, 94, 79, 16);
		contentPane.add(lblProducer);
		
		txtPublisher = new JTextField();
		txtPublisher.setColumns(10);
		txtPublisher.setBounds(149, 91, 199, 22);
		contentPane.add(txtPublisher);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(58, 136, 79, 16);
		contentPane.add(lblPrice);
		
		txtPrice = new JTextField();
		txtPrice.setColumns(10);
		txtPrice.setBounds(149, 133, 199, 22);
		contentPane.add(txtPrice);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String sql = "insert into books(name_book,author,publisher,price) values(?,?,?,?)";
				try {
					PreparedStatement statement = connect.prepareStatement(sql);
					statement.setString(1, txtNameBook.getText());
					statement.setString(2, txtAuthor.getText());
					statement.setString(3, txtPublisher.getText());
					statement.setString(4, txtPrice.getText());
					int kq = new JOptionPane().showConfirmDialog(rootPane, "Do you want add book?","Confirm",JOptionPane.YES_NO_OPTION);
					if(kq == 0) {
						int resultset = statement.executeUpdate();
							if (resultset > 0) {
								new JOptionPane().showMessageDialog(rootPane, "Add book successfully");
							} else {
								new JOptionPane().showMessageDialog(rootPane, "Can't add book");
							}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					new JOptionPane().showMessageDialog(rootPane, e);
				}
			}
		});
		btnAdd.setBounds(97, 180, 97, 25);
		contentPane.add(btnAdd);
		
		JButton btnRenew = new JButton("Undo");
		btnRenew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtNameBook.setText("");
				txtAuthor.setText("");
				txtPublisher.setText("");
				txtPrice.setText("");
			}
		});
		btnRenew.setBounds(241, 180, 97, 25);
		contentPane.add(btnRenew);
	}
}
