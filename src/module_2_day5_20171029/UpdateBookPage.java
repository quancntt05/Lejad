package module_2_day5_20171029;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class UpdateBookPage extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtAuthor;
	private JTextField txtPublisher;
	private JTextField txtPrice;
	private JTable table;
	private String idxUpdateBook = "";
	UpdateBookPage that = this;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateBookPage frame = new UpdateBookPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UpdateBookPage() {
		ConnectDB db = new ConnectDB();
		Connection connect = db.connect();
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				that.setVisible(false);
				db.disconnect();
			}
		});
		
		setBounds(100, 100, 546, 523);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNameBook = new JLabel("Name book");
		lblNameBook.setBounds(64, 29, 90, 16);
		contentPane.add(lblNameBook);
		
		txtName = new JTextField();
		txtName.setBounds(157, 26, 164, 22);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(64, 74, 90, 16);
		contentPane.add(lblAuthor);
		
		txtAuthor = new JTextField();
		txtAuthor.setColumns(10);
		txtAuthor.setBounds(157, 71, 164, 22);
		contentPane.add(txtAuthor);
		
		JLabel lblPuslish = new JLabel("Publisher");
		lblPuslish.setBounds(64, 120, 90, 16);
		contentPane.add(lblPuslish);
		
		txtPublisher = new JTextField();
		txtPublisher.setColumns(10);
		txtPublisher.setBounds(157, 117, 164, 22);
		contentPane.add(txtPublisher);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(64, 166, 90, 16);
		contentPane.add(lblPrice);
		
		txtPrice = new JTextField();
		txtPrice.setColumns(10);
		txtPrice.setBounds(157, 163, 164, 22);
		contentPane.add(txtPrice);
		
		JScrollPane jScrollPane = new JScrollPane();
		jScrollPane.setBounds(46, 233, 442, 230);
		contentPane.add(jScrollPane);
		
		table = new JTable();
		jScrollPane.setViewportView(table);
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Id");
		model.addColumn("Book name");
		model.addColumn("Author");
		model.addColumn("Publisher");
		model.addColumn("Price");
		table.setModel(model);
		
		table.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				txtName.setText(String.valueOf(model.getValueAt(table.getSelectedRow(),1)));
				txtAuthor.setText(String.valueOf(model.getValueAt(table.getSelectedRow(),2)));
				txtPublisher.setText(String.valueOf(model.getValueAt(table.getSelectedRow(),3)));
				txtPrice.setText(String.valueOf(model.getValueAt(table.getSelectedRow(),4)));
				idxUpdateBook = String.valueOf(model.getValueAt(table.getSelectedRow(),0));	
			}
		});
		
		JButton btnList = new JButton("List Book");
		btnList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Books> listBooks = new ArrayList<>();
				listBooks = ListBook();
				if (listBooks.size() == 0) {
					new JOptionPane().showMessageDialog(rootPane, "Not have book");
				}else {
					for (Books books : listBooks) {
						model.addRow(new String[] {String.valueOf(books.getId()),books.getNameBook(),books.getAuthor(),books.getPublisher(),books.getPrice()});
					}
				}
			}

			private List<Books> ListBook() {
				// TODO Auto-generated method stub
				List<Books> list = new ArrayList<>();
				String sql = "select * from books";
				try {
					PreparedStatement statement = connect.prepareStatement(sql);
					ResultSet result = statement.executeQuery();
					while (result.next()) {
						list.add(new Books(result.getInt(5), result.getString(1), result.getString(2), result.getString(3), result.getString(4)));
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return list;
			}
		});
		btnList.setBounds(375, 54, 97, 25);
		contentPane.add(btnList);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int result = new JOptionPane().showConfirmDialog(rootPane, "Do you want update "+txtName.getText()+" ?","Comfirm",JOptionPane.YES_NO_OPTION);
				if(result == 0) {
					new JOptionPane().showMessageDialog(rootPane, UpdateBook(idxUpdateBook));
				}
			}
			private String UpdateBook(String id) {
				// TODO Auto-generated method stub
				String result = "";
				String sql = String.format("update books set name_book = '%s', author = '%s', publisher = '%s', price = '%s' where id = '%s'",
						txtName.getText(),txtAuthor.getText(),txtPublisher.getText(),txtPrice.getText(),id);
				PreparedStatement statement;
				try {
					statement = connect.prepareStatement(sql);
					int update = statement.executeUpdate();
					if(update>0) {
						result = "Updated";
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return result;
			}
		});
		btnUpdate.setBounds(375, 116, 97, 25);
		contentPane.add(btnUpdate);
	}

}
