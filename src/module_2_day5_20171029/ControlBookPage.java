package module_2_day5_20171029;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ControlBookPage extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ControlBookPage frame = new ControlBookPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ControlBookPage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Add Book");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddNewBookPage addNewBookPage = new AddNewBookPage();
				addNewBookPage.setVisible(true);
			}
		});
		btnNewButton.setBounds(95, 23, 204, 52);
		contentPane.add(btnNewButton);
		
		JButton btnUpdateBook = new JButton("Update Book");
		btnUpdateBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateBookPage updateBookPage = new UpdateBookPage();
				updateBookPage.setVisible(true);
			}
		});
		btnUpdateBook.setBounds(95, 101, 204, 52);
		contentPane.add(btnUpdateBook);
		
		JButton btnFindOrDelete = new JButton("Find or Delete Book");
		btnFindOrDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FindAndDelBook findAndDelBook = new FindAndDelBook();
				findAndDelBook.setVisible(true);
			}
		});
		btnFindOrDelete.setBounds(95, 176, 204, 52);
		contentPane.add(btnFindOrDelete);
	}
}
