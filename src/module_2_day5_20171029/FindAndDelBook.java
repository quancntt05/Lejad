package module_2_day5_20171029;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class FindAndDelBook extends JFrame {

	private JPanel contentPane;
	private JTextField txtFind;
	private JTable table;
	String idxDelBook = "";
	FindAndDelBook that = this;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FindAndDelBook frame = new FindAndDelBook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FindAndDelBook() {
		ConnectDB db = new ConnectDB();
		Connection connect = db.connect();
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				db.disconnect();
				that.setVisible(false);
			}
		});

		setBounds(100, 100, 699, 485);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFindBook = new JLabel("Find book");
		lblFindBook.setBounds(128, 34, 65, 16);
		contentPane.add(lblFindBook);
		
		txtFind = new JTextField();
		txtFind.setBounds(205, 31, 283, 22);
		contentPane.add(txtFind);
		txtFind.setColumns(10);
		
		JScrollPane jScrollPane = new JScrollPane();
		jScrollPane.setBounds(69, 136, 535, 256);
		contentPane.add(jScrollPane);
		
		table = new JTable();
		jScrollPane.setViewportView(table);
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Id");
		model.addColumn("Book name");
		model.addColumn("Author");
		model.addColumn("Publisher");
		model.addColumn("Price");
		table.setModel(model);
		
		table.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				txtFind.setText(String.valueOf(model.getValueAt(table.getSelectedRow(),1)));
				idxDelBook = String.valueOf(model.getValueAt(table.getSelectedRow(),0));						
			}
		});
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Get book from db
					List<Books> listBooks = new ArrayList<>();
					listBooks = ListBook(txtFind.getText());
					if (listBooks.size() == 0) {
						new JOptionPane().showMessageDialog(rootPane, "Book Not Found");
					}else {
						for (Books books : listBooks) {
							model.addRow(new String[] {String.valueOf(books.getId()),books.getNameBook(),books.getAuthor(),books.getPublisher(),books.getPrice()});
						}
					}
			}

			private List<Books> ListBook(String text) {
				// TODO Auto-generated method stub
				List<Books> list = new ArrayList<>();
				String sql = "select * from books where name_book like '%"+text+"%'";
				try {
					PreparedStatement statement = connect.prepareStatement(sql);
					ResultSet result = statement.executeQuery();
					while (result.next()) {
						list.add(new Books(result.getInt(5), result.getString(1), result.getString(2), result.getString(3), result.getString(4)));
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return list;
			}
		});
		btnSearch.setBounds(185, 81, 97, 25);
		contentPane.add(btnSearch);
			
		JButton btnUndo = new JButton("Del Book");
		btnUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int result = new JOptionPane().showConfirmDialog(rootPane, "Do you want delete "+txtFind.getText()+" ?","Comfirm",JOptionPane.YES_NO_OPTION);
				if(result == 0) {
					new JOptionPane().showMessageDialog(rootPane, DeleteBook(idxDelBook));
				}
			}
			private String DeleteBook(String id) {
				// TODO Auto-generated method stub
				String result = "";
				String sql = "delete from books where id = ?";
				PreparedStatement statement;
				try {
					statement = connect.prepareStatement(sql);
					statement.setString(1, id);
					int update = statement.executeUpdate();
					if(update>0) {
						result = "Deleted";
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return result;
			}
		});
		btnUndo.setBounds(383, 81, 97, 25);
		contentPane.add(btnUndo);
	}
}
