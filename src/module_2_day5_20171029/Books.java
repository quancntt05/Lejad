package module_2_day5_20171029;

public class Books {
	public Books(int id, String nameBook, String author, String publisher, String price) {
		super();
		this.nameBook = nameBook;
		this.author = author;
		this.publisher = publisher;
		this.price = price;
		this.id = id;
	}
	public String getNameBook() {
		return nameBook;
	}
	public void setNameBook(String nameBook) {
		this.nameBook = nameBook;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private String nameBook;
	private String author;
	private String publisher;
	private String price;
	private int id;
	
	
}
